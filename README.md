## Requirements

For development, you will only need Node.js installed on your environement. And please use the appropriate Editorconfig plugin for your Editor (not mandatory).

### Node

Node is really easy to install & now include NPM. You should be able to run the following command after the installation procedure below.


```
$ node --version
v12.14.1

$ npm --version
6.14.4
```

### Node installation on OS X
You will need to use a Terminal. On OS X, you can find the default terminal in `/Applications/Utilities/Terminal.app`.

Please install Homebrew if it's not already done with the following command.
```
$ ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
```
If everything when fine, you should run
```
brew install node
```
### Node installation on Linux
```
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs
```
### Install
```
$ npm install
```

### Start & watch
```
$ npm start
```

## Note

- Implemented the dashboard with which the user can manage his account.
- I have added two tabs one is for account setting and second one is for user information.
- I have used react-redux, redux-thunk, fontawesome, scss, redux-form, eslint and prettier.
- Code is properly structured and divided to improve code reusability.
- For API calls I have used axios package.
- For quick notification, I have used react-redux-toastr, currently it is showing error message on form submission as API end point is not specified but once it is added, it will work perfectly fine.
- It is properly mobile responsive for all size of devices.
- I have spend 7-8 hours on this task due to some extra bonus work.
