import axios from 'axios'

const URL = 'BACKEND_URL'

export const updateAccountSetting = (payload, successAction, failureAction) => (dispatch) => {
  axios
    .get(URL)
    .then((response) => {
      dispatch(successAction(response))
    })
    .catch((error) => {
      dispatch(failureAction(error))
    })
}

export const updateUserInformation = (payload, successAction, failureAction) => (dispatch) => {
  axios
    .get(URL)
    .then((response) => {
      dispatch(successAction(response))
    })
    .catch((error) => {
      dispatch(failureAction(error))
    })
}
