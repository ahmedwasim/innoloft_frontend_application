import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faHome,
  faBullhorn,
  faBuilding,
  faCog,
  faNewspaper,
  faChartArea,
} from '@fortawesome/free-solid-svg-icons'

import './style.scss'

export default () => (
  <div className="sidebar-main-container">
    <ul>
      <li>
        <FontAwesomeIcon icon={faHome} />
        <span className="label">Home</span>
      </li>
      <li>
        <FontAwesomeIcon icon={faBullhorn} />
        <span className="label">My Account</span>
      </li>
      <li>
        <FontAwesomeIcon icon={faBuilding} />
        <span className="label">My Company</span>
      </li>
      <li>
        <FontAwesomeIcon icon={faCog} />
        <span className="label">My Settings</span>
      </li>
      <li>
        <FontAwesomeIcon icon={faNewspaper} />
        <span className="label">News</span>
      </li>
      <li>
        <FontAwesomeIcon icon={faChartArea} />
        <span className="label">Analytics</span>
      </li>
    </ul>
  </div>
)
