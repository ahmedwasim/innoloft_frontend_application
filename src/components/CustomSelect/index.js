import React from 'react'

import './style.scss'

const countryList = [
  { label: ' - ', value: '' },
  { label: 'Germany', value: 'germany' },
  { label: 'Austria', value: 'austria' },
  { label: 'Switzerland', value: 'switzerland' },
]

export default ({ input, label, type, showAllErrors, meta: { touched, error, warning } }) => (
  <div className="select-main-container">
    <div>
      <label htmlFor={input.name}>{label}: </label>
      <div>
        <select type={type} placeholder={label} value={input.value} {...input}>
          {countryList.map((list) => (
            <option key={list.value} value={list.value}>
              {list.label}
            </option>
          ))}
        </select>
        {(touched || showAllErrors) &&
          ((error && <div>{error}</div>) || (warning && <div>{warning}</div>))}
      </div>
    </div>
  </div>
)
