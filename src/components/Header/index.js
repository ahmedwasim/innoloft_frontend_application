import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGlobeAsia, faEnvelope, faBell } from '@fortawesome/free-solid-svg-icons'

import './style.scss'

export default () => (
  <div className="header-main-container">
    <ul>
      <div>
        <li style={{ padding: 0 }}>
          <img src="/energieloft_logo.png" alt="logo" />
        </li>
      </div>

      <div>
        <li>
          <FontAwesomeIcon icon={faGlobeAsia} />
        </li>
        <li>
          <FontAwesomeIcon icon={faEnvelope} />
        </li>
        <li>
          <FontAwesomeIcon className="last" icon={faBell} />
        </li>
      </div>
    </ul>
  </div>
)
