import React from 'react'
import PasswordStrengthBar from 'react-password-strength-bar'

import './style.scss'

export default ({
  input,
  label,
  type,
  password,
  showAllErrors,
  meta: { touched, error, warning },
}) => {
  return (
    <div className="input-main-container">
      <div>
        <label htmlFor={input.name}>{label}: </label>
        <div>
          <input type={type} placeholder={label} value={input.value} {...input} />
          {password && <PasswordStrengthBar password={password} />}
          {(touched || showAllErrors) &&
            ((error && <div>{error}</div>) || (warning && <div>{warning}</div>))}
        </div>
      </div>
    </div>
  )
}
