import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'

import './index.css'
import * as serviceWorker from './serviceWorker'
import App from './App'
import configureStore from './store/configureStore'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'

const store = configureStore()

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
