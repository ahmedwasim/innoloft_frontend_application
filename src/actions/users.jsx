import { toastr } from 'react-redux-toastr'

import * as types from './actionTypes'

export const updateAccountSettingSuccess = (payload) => (dispatch) => {
  dispatch({
    type: types.UPDATE_ACCOUNT_SETTING_SUCCESS,
    payload: payload,
  })

  toastr.success('Changes Saved Successfully!')
}

export const updateAccountSettingFailure = (payload) => (dispatch) => {
  dispatch({
    type: types.UPDATE_ACCOUNT_SETTING_FAILURE,
    payload,
  })

  toastr.error('Failed to save changes!')
}

export const updateUserinfomationsSuccess = (payload) => (dispatch) => {
  dispatch({
    type: types.UPDATE_USER_INFORMATION_SUCCESS,
    payload: payload,
  })

  toastr.success('Changes Saved Successfully!')
}

export const updateUserinfomationsFailure = (payload) => (dispatch) => {
  dispatch({
    type: types.UPDATE_USER_INFORMATION_FAILURE,
    payload,
  })

  toastr.error('Failed to save changes!')
}
