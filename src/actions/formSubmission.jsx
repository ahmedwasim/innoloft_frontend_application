import * as types from './actionTypes'

export const enableFormSubmission = {
  type: types.ENABLE_FORM_SUBMISSION,
}

export const disableFormSubmission = {
  type: types.DISABLE_FORM_SUBMISSION,
}
