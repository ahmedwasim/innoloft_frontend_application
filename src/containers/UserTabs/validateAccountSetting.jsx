/* eslint-disable */

export const validate = (values) => {
  const errors = {}

  if (!values.email) {
    errors.email = 'Required'
  } else {
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (!regex.test(values.email)) {
      errors.email = 'Invalid value!'
    }
  }

  if (!values.password) {
    errors.password = 'Required'
  }

  if (!values.password_confirmation) {
    errors.password_confirmation = 'Required'
  }

  if (values.password && values.password_confirmation) {
    if (values.password !== values.password_confirmation) {
      errors.password_confirmation = 'Passwords should match!'
    }
  }

  return errors
}

export default validate
