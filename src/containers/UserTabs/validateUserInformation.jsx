export const validate = (values) => {
  const errors = {}

  if (!values.first_name) {
    errors.first_name = 'Required'
  } else if (!/^[a-zA-Z]+$/.test(values.first_name)) {
    errors.first_name = 'Invalid value!'
  }

  if (!values.last_name) {
    errors.last_name = 'Required'
  } else if (!/^[a-zA-Z]+$/.test(values.last_name)) {
    errors.last_name = 'Invalid value!'
  }

  if (!values.country) {
    errors.country = 'Required'
  }

  if (!values.street) {
    errors.street = 'Required'
  } else if (values.street.length > 10) {
    errors.street = 'Invalid value!'
  }

  if (!values.house_number) {
    errors.house_number = 'Required'
  } else if (values.house_number.length > 4) {
    errors.house_number = 'Invalid value!'
  }

  if (!values.postal_code) {
    errors.postal_code = 'Required'
  } else if (!/^d{5}-\d{4}|\d{5}|[A-Z]\d[A-Z] \d[A-Z]\d$/.test(values.postal_code)) {
    errors.postal_code = 'Invalid value!'
  }

  return errors
}

export default validate
