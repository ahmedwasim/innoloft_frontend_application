import React from 'react'
import { Field } from 'redux-form'
import CustomInput from '../../../components/CustomInput'

export default ({ showAllErrors }) => (
  <form>
    <Field
      name="street"
      placeholder="Street"
      label="Street"
      type="text"
      required="required"
      component={CustomInput}
      showAllErrors={showAllErrors}
    />

    <Field
      name="house_number"
      placeholder="House Number"
      label="House Number"
      type="text"
      component={CustomInput}
      showAllErrors={showAllErrors}
    />

    <Field
      name="postal_code"
      placeholder="Postal Code"
      label="Postal Code"
      type="text"
      component={CustomInput}
      showAllErrors={showAllErrors}
    />
  </form>
)
