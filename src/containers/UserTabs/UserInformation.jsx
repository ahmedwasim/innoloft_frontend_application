import React, { Component } from 'react'
import { Field } from 'redux-form'
import { reduxForm } from 'redux-form'

import Address from './Address'
import { connect } from 'react-redux'
import CustomInput from '../../components/CustomInput'
import CustomSelect from '../../components/CustomSelect'
import validate from './validateUserInformation'
import './styles.scss'

class UserInformation extends Component {
  state = {
    showAllErrors: false,
  }

  handleSubmit = (event, values) => {
    const { handleSubmit, submission, valid } = this.props

    event.preventDefault()

    if (valid && !submission) {
      handleSubmit(values)
    } else {
      this.setState({ showAllErrors: true })
    }
  }

  render() {
    const { submission } = this.props
    const { showAllErrors } = this.state

    return (
      <form onSubmit={this.handleSubmit} className="user-information">
        <h3> User Informations </h3>
        <hr />

        <Field
          name="first_name"
          placeholder="First Name"
          label="First Name"
          type="text"
          required="required"
          component={CustomInput}
          showAllErrors={showAllErrors}
        />

        <Field
          name="last_name"
          placeholder="Last Name"
          label="Last Name"
          type="text"
          required="required"
          component={CustomInput}
          showAllErrors={showAllErrors}
        />

        <Address showAllErrors={showAllErrors} />

        <Field
          name="country"
          placeholder="Country"
          label="Country"
          type="text"
          required="required"
          component={CustomSelect}
          showAllErrors={showAllErrors}
        />

        <div className="button">
          <button disabled={submission} onClick={this.handleSubmit} type="submit" color="primary">
            Save
          </button>
        </div>
      </form>
    )
  }
}

const mapStateToProps = (state) => ({
  submission: state.formSubmission.submission,
})

UserInformation = reduxForm({
  form: 'user_information',
  validate,
})(UserInformation)

export default connect(mapStateToProps)(UserInformation)
