import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field } from 'redux-form'
import { reduxForm, getFormValues } from 'redux-form'

import CustomInput from '../../components/CustomInput'
import validate from './validateAccountSetting'
import './styles.scss'

class AccountSettings extends Component {
  state = {
    showAllErrors: false,
  }

  handleSubmit = (event, values) => {
    const { formValues, valid, submission, handleSubmit } = this.props

    event.preventDefault()

    if (valid && !submission) {
      handleSubmit(formValues)
    } else {
      this.setState({ showAllErrors: true })
    }
  }

  render() {
    const { formValues, submission } = this.props
    const { showAllErrors } = this.state

    return (
      <form onSubmit={this.handleSubmit} className="account-form">
        <h3> Account Settings </h3>
        <hr />

        <Field
          name="email"
          placeholder="Email"
          label="Email"
          type="text"
          required="required"
          component={CustomInput}
          showAllErrors={showAllErrors}
        />

        <Field
          name="password"
          placeholder="Password"
          label="Password"
          type="password"
          required="required"
          password={formValues && formValues['password']}
          component={CustomInput}
          showAllErrors={showAllErrors}
        />

        <Field
          name="password_confirmation"
          placeholder="Password Confirmation"
          label="Password Confirmation"
          type="password"
          required="required"
          password={formValues && formValues['password_confirmation']}
          component={CustomInput}
          showAllErrors={showAllErrors}
        />

        <div className="button">
          <button disabled={submission} onClick={this.handleSubmit} type="submit" color="primary">
            Save
          </button>
        </div>
      </form>
    )
  }
}

const mapStateToProps = (state) => ({
  formValues: getFormValues('account_settings')(state),
  submission: state.formSubmission.submission,
})

AccountSettings = reduxForm({
  form: 'account_settings',
  touchOnChange: true,
  validate,
})(AccountSettings)

export default connect(mapStateToProps)(AccountSettings)
