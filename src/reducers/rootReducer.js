import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { reducer as toastrReducer } from 'react-redux-toastr'

import formSubmission from './formSubmission'

const rootReducer = combineReducers({
  form: formReducer,
  formSubmission: formSubmission,
  toastr: toastrReducer,
})

export default rootReducer
