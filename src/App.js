import React from 'react'
import Collapsible from 'react-collapsible'
import { connect } from 'react-redux'
import { getFormValues } from 'redux-form'
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ReduxToastr from 'react-redux-toastr'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import 'react-tabs/style/react-tabs.css'

import './App.scss'
import AccountSettings from './containers/UserTabs/AccountSettings'
import Header from './components/Header'
import Sidebar from './components/Sidebar'
import UserInformation from './containers/UserTabs/UserInformation'
import { updateAccountSetting, updateUserInformation } from './api'
import {
  updateAccountSettingSuccess,
  updateAccountSettingFailure,
  updateUserinfomationsSuccess,
  updateUserinfomationsFailure,
} from './actions/users'

const App = (props) => {
  const handleUpdateAccountSetting = (values) => {
    props.updateAccountSetting(values)
  }

  const handleUserInformation = (values) => {
    props.updateAccountSetting(values)
  }

  return (
    <div>
      <Header />

      <div className="main-container">
        <Collapsible
          trigger={
            <div className="menu-bar">
              <span
                style={{
                  color: '#999',
                  display: 'flex',
                  backgroundColor: '#e8e8e8',
                  padding: 10,
                  borderRadius: 5,
                }}
              >
                <FontAwesomeIcon icon={faBars} />
                <span> Menu </span>
              </span>
            </div>
          }
        >
          <Sidebar />
        </Collapsible>

        <Sidebar />

        <div className="tab-container">
          <Tabs>
            <TabList>
              <Tab>Account Settings</Tab>
              <Tab>User Information</Tab>
            </TabList>

            <TabPanel>
              <AccountSettings handleSubmit={handleUpdateAccountSetting} />
            </TabPanel>

            <TabPanel>
              <UserInformation handleSubmit={handleUserInformation} />
            </TabPanel>
          </Tabs>
        </div>
      </div>

      <ReduxToastr
        timeOut={4000}
        newestOnTop={false}
        preventDuplicates
        position="top-right"
        transitionIn="fadeIn"
        transitionOut="fadeOut"
        progressBar
        closeOnToastrClick
      />
    </div>
  )
}

const mapStateToProps = (state) => ({
  formValues: getFormValues('account_settings')(state),
})

const mapDispatchToProps = (dispatch) => ({
  updateAccountSetting: (params) => {
    dispatch(updateAccountSetting(params, updateAccountSettingSuccess, updateAccountSettingFailure))
  },
  udpateuserInformations: (params) => {
    dispatch(
      updateUserInformation(params, updateUserinfomationsSuccess, updateUserinfomationsFailure)
    )
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
